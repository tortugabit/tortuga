pragma solidity ^0.4.23;

import "./ERC721.sol";


/**
 * @title Full ERC721 Token
 * This implementation includes all the required and some optional functionality of the ERC721 standard
 * Moreover, it includes approve all functionality using operator terminology
 * @dev see https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
contract BaseToken is ERC721 {

  string public name;
  string public symbol;

  // Mapping from owner to list of owned token IDs
  mapping (address => uint[]) internal ownedTokens;

  // Mapping from token ID to index of the owner tokens list
  mapping(uint => uint) internal ownedTokensIndex;

  // Array with all token ids, used for enumeration
  uint[] internal allTokens;

  // Mapping from token id to position in the allTokens array
  mapping(uint => uint) internal allTokensIndex;

  // Optional mapping for token URIs
  mapping(uint => string) internal tokenURIs;

  constructor (string _name, string _symbol) public {
    name = _name;
    symbol = _symbol;
  }

  function tokenURI(uint _tokenId) public view returns (string) {
    require(exists(_tokenId));
    return tokenURIs[_tokenId];
  }

  function tokenOfOwnerByIndex(address _owner, uint _index) public view returns (uint) {
    require(_index < balanceOf(_owner));
    return ownedTokens[_owner][_index];
  }


  function totalSupply() public view returns (uint) {
    return allTokens.length;
  }


  function tokenByIndex(uint _index) public view returns (uint) {
    require(_index < totalSupply());
    return allTokens[_index];
  }


  function _setTokenURI(uint _tokenId, string _uri) internal {
    require(exists(_tokenId));
    tokenURIs[_tokenId] = _uri;
  }

  function addTokenTo(address _to, uint _tokenId) internal {
    super.addTokenTo(_to, _tokenId);
    uint length = ownedTokens[_to].length;
    ownedTokens[_to].push(_tokenId);
    ownedTokensIndex[_tokenId] = length;
  }

  function removeTokenFrom(address _from, uint _tokenId) internal {
    super.removeTokenFrom(_from, _tokenId);

    uint tokenIndex = ownedTokensIndex[_tokenId];
    uint lastTokenIndex = ownedTokens[_from].length.sub(1);
    uint lastToken = ownedTokens[_from][lastTokenIndex];

    ownedTokens[_from][tokenIndex] = lastToken;
    ownedTokens[_from][lastTokenIndex] = 0;

    ownedTokens[_from].length--;
    ownedTokensIndex[_tokenId] = 0;
    ownedTokensIndex[lastToken] = tokenIndex;
  }

  function _mint(address _to, uint _tokenId) internal {
    super._mint(_to, _tokenId);

    allTokensIndex[_tokenId] = allTokens.length;
    allTokens.push(_tokenId);
  }

  function _burn(address _owner, uint _tokenId) internal {
    super._burn(_owner, _tokenId);

    // Clear metadata (if any)
    if (bytes(tokenURIs[_tokenId]).length != 0) {
      delete tokenURIs[_tokenId];
    }

    // Reorg all tokens array
    uint tokenIndex = allTokensIndex[_tokenId];
    uint lastTokenIndex = allTokens.length.sub(1);
    uint lastToken = allTokens[lastTokenIndex];

    allTokens[tokenIndex] = lastToken;
    allTokens[lastTokenIndex] = 0;

    allTokens.length--;
    allTokensIndex[_tokenId] = 0;
    allTokensIndex[lastToken] = tokenIndex;
  }

}
