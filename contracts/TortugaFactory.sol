pragma solidity ^0.4.23;

import './ERC721BaseToken.sol';


contract TortugaFactory is  ERC721BaseToken{

  event CreateCollection(uint id, address indexed author, string name, uint totalInSeries);
  event AddEntity(uint collectionId, address indexed author, uint8 entityId);

  /**
  * Description Collection Token
  */
  struct Collection {
    string name;
    uint8 totalInSeries;
    uint dateCreation;
    address author;
  }

  /**
  *  EntityTokens
  */
  struct Entity {
    uint collectionId;
    string URI;
  }

  Collection[] internal collections;
  Entity[] internal entities;

  mapping (uint => uint8) internal countEntityInCollection;

  function _createCollection(string name, uint8 totalInSeries) internal returns (bool){
    address author = msg.sender;
    uint id = collections.push(Collection(name, totalInSeries, now, author)) - 1;
    emit CreateCollection(id, author, name, totalInSeries);

    return true;
  }

  function _addEntity(uint collectionId, string URI) internal returns (bool){

    Collection memory collection = collections[collectionId];
    require(countEntityInCollection[collectionId] <= collection.totalInSeries);
    require(collection.author == msg.sender);

    address author = msg.sender;
    uint id = entities.push(Entity(collectionId, URI)) - 1;
    countEntityInCollection[collectionId]++;

    addTokenTo(author, id);

    emit AddEntity(collectionId, author, uint8(id));
    return true;
  }

}
