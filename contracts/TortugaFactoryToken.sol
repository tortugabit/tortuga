pragma solidity ^0.4.23;

import './TortugaFactory.sol';


contract TortugaFactoryToken is TortugaFactory{

  /*mapping (address => uint[]) internal collectionOwner;
  mapping (address => mapping(uint => uint[])) internal entityOwner;*/

  constructor() {
    name="Tortuga";
    symbol="TTG";
  }


  function createCollection(string name, uint8 totalInSeries)  public  returns(bool){
    return _createCollection(name, totalInSeries);
  }

  function addEntity(uint collectionId, string action, string URI) public  returns(bool){
    _addEntity(uint collectionId, string URI);
    return true;
  }



  function getCollectionEntity(
    uint collectionId,
    uint entityId
  )
  external view
  returns(
    string collectionName,
    string entityAction,
    string entityMeta,
    uint8 totalEntity,
    address author
  )
  {
    Collection memory collection = collections[collectionId];
    Entity memory  entity = entities[entityId];

    collectionName = collection.name;
    totalEntity = collection.totalInSeries;
    author = collection.author;
    //entityAction = entity.action;
    entityMeta = entity.URI;
  }



  function getCollectionsIdByOwner(address _owner) external view returns(uint[]){
    /*uint[] memory collections= ownedTokens[_owner];
    return collections;*/
  }

  function getEntitiesIdByOwner(address _owner, uint collectionId) external view returns(uint[]){
    /*uint[] memory entities =  ownedTokens[_owner];
    return entities;*/
  }

/*
function setEntityOwner(address _user, string action) public{

for(uint i=0; i < entities.length; i++){

Entity memory entity = entities[i];
if(keccak256(entity.action) == keccak256(action)){

if(collectionToOwnerCount[_user][entity.collectionId] == 0){
collectionToOwnerCount[_user][entity.collectionId]++;
collectionOwner[_user].push(entity.collectionId);
}

if(entityToOwnerCount[_user][i] == 0){
entityToOwnerCount[_user][i]++;
entityOwner[_user][entity.collectionId].push(i);
}

}
}
}*/
}


