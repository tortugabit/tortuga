pragma solidity ^0.4.23;

import './ERC721BaseToken.sol';

contract TortugaFactory is BaseToken{

    event CreateCollection(uint id, address indexed author, string name, uint count);
    event AddEntity(uint collectionId, address indexed author, string action, string meta);

    struct Collection {
        string name;
        uint count;
    }

    struct Entity {
        uint collectionId;
        string action;
        string meta;
    }

    Collection[] internal collections;
    Entity[] internal entities;

    mapping (uint => address) internal collectionToAuthor;
    mapping (address => mapping(uint => uint)) internal entityToAuthorCount;

    mapping (address => uint[]) internal collectionOwner;
    mapping (address => mapping(uint => uint[])) internal entityOwner;

    mapping (address => mapping(uint => uint)) internal collectionToOwnerCount;
    mapping (address => mapping(uint => uint)) internal entityToOwnerCount;


    function _createCollection(string name, uint count) internal returns (uint){

        uint id = collections.push(Collection(name, count)) - 1;

        collectionToAuthor[id]= msg.sender;
        return id;

    }

    function _addEntity(uint collectionId, string action, string meta) internal returns (uint){
        uint id = entities.push(Entity(collectionId, action, meta)) - 1;
        entityToAuthorCount[msg.sender][collectionId]++;

        emit AddEntity(collectionId, msg.sender, action, meta);

        return id;
    }

}


contract TortugaFactoryToken is TortugaFactory{

    function createCollection(string name, uint count)  public  returns(bool){
        uint id =_createCollection(name, count);
        collectionOwner[msg.sender].push(id);
        return true;
    }

    function addEntity(uint collectionId, string action, string meta) public  returns(bool){
        Collection memory collection = collections[collectionId];
        uint entityTotal = entityToAuthorCount[msg.sender][collectionId];
        require(collection.count > entityTotal);
        require(collectionToAuthor[collectionId] == msg.sender);

        uint id =_addEntity(collectionId, action, meta);
        entityOwner[msg.sender][collectionId].push(id);
        return true;
    }

    function setEntityOwner(address _user, string action) public{

        for(uint i=0; i < entities.length; i++){

            Entity memory entity = entities[i];
            if(keccak256(entity.action) == keccak256(action)){

                if(collectionToOwnerCount[_user][entity.collectionId] == 0){
                    collectionToOwnerCount[_user][entity.collectionId]++;
                    collectionOwner[_user].push(entity.collectionId);
                }


                if(entityToOwnerCount[_user][i] == 0){
                    entityToOwnerCount[_user][i]++;
                    entityOwner[_user][entity.collectionId].push(i);
                }

            }
        }
    }

    function getCollectionEntity(uint collectionId, uint entityId) external view
    returns(string collectionName, string entityAction, string entityMeta, uint totalEntity, address author)
    {
        Collection memory collection = collections[collectionId];
        Entity memory  entity = entities[entityId];

        collectionName = collection.name;
        entityAction = entity.action;
        entityMeta = entity.meta;
        totalEntity = collection.count;
        author = collectionToAuthor[collectionId];
    }



    function getCollectionsIdByOwner(address _owner) external view
    returns(uint[])
    {
        uint[] memory collections= collectionOwner[_owner];
        return collections;
    }

    function getEntitiesIdByOwner(address _owner, uint collectionId) external view
    returns(uint[])
    {
        uint[] memory entities =  entityOwner[_owner][collectionId];
        return entities;
    }

}